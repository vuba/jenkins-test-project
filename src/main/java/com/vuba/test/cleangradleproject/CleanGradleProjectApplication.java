package com.vuba.test.cleangradleproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CleanGradleProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(CleanGradleProjectApplication.class, args);
	}
}
